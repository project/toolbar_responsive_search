Toolbar Responsive Search

Adds a site-wide search form (for now, relying on the core search module) to the
toolbar. At small screens, it's a separate toolbar tab where the search form
lives in the toolbar tray. At wider screens, the tab/tray disappear and the
search form is directly visible in the toolbar.

Requires core modules:
- toolbar
- search
